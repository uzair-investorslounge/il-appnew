import 'package:flutter/material.dart';
import 'package:investors_lounge/static/screens/starting_screens/intro_slider/slider.dart';


import 'package:investors_lounge/static/screens/starting_screens/login.dart';

void main() {
  runApp(MaterialApp(
    color: Colors.white,
    theme: ThemeData(
      fontFamily: 'Roboto',
    ),
    debugShowCheckedModeBanner: false,
    home: SliderScreen(),
  ));
}

