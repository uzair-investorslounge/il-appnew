import 'package:flutter/material.dart';
import 'package:investors_lounge/static/commons/common.dart';
import 'package:investors_lounge/static/screens/tabbar/market/indices.dart';

class Market extends StatefulWidget {
  @override
  _MarketState createState() => _MarketState();
}

class _MarketState extends State<Market> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            labelColor: primary,
            labelStyle: TextStyle(fontSize: 16),
            unselectedLabelColor: Colors.black54,
            indicatorColor: primary,
            indicatorPadding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
            labelPadding: EdgeInsets.symmetric(horizontal: 0),
            tabs: [
              Tab(
                text: "Indices",
              ),
              Tab(
                text: "Screener",
              ),
              Tab(
                text: "Results",
              ),
              Tab(
                text: "Meetings",
              ),
              // Tab(
              //   child: Text(
              //     "News", style: TextStyle(fontSize: 11),
              //   ),
              // ),
            ],
          ),
          backgroundColor: Colors.white,
          elevation: 0.0,
          leading: IconButton(
              icon: Icon(
                Icons.menu,
                color: Color(0xff25cd9c),
              ),
              onPressed: () {}),
          actions: <Widget>[
            IconButton(
                icon: Icon(
                  Icons.search,
                  color: Color(0xff25cd9c),
                ),
                onPressed: () {}),
          ],
        ),
        body: Padding(
          padding: EdgeInsets.all(0.0),
          child: TabBarView(
            children: [
              Indices(),
              Text("b"),
              Text("c"),
              Text("d"),
              //Text("News"),
            ],
          ),
        ),
      ),
    );
  }
}
