import 'package:flutter/material.dart';
import 'package:investors_lounge/static/commons/common.dart';
//import 'package:investors_lounge/static/screens/tabbar/news/news_view.dart';
//import 'file:///E:/investors_lounge/app/il-appnew/App/investors_app/lib/static/widgets/news_and_research/news_list.dart';
import 'package:investors_lounge/static/widgets/news/news_list.dart';

import 'news/news_view.dart';

class ListItems extends StatefulWidget {
  @override
  _ListItemsAllNewsState createState() => _ListItemsAllNewsState();
}

class _ListItemsAllNewsState extends State<ListItems> {
  @override
  Widget build(BuildContext context) {
    //HEIGHT-WIDTH
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          SizedBox(
            height: height * 0.02,
          ),
          NewsList(
            title: "Gas pipeline projects among PM's Russia visit agenda",
            subtitle: "The Tribune Express",
            time: ". 40m",
            imagepath: "images/test.jpg",
            onclick: (){
              changeScreen(context, NewsView());
            },
          ),
          SizedBox(
            height: height * 0.02,
          ),
          NewsList(
            title: "Exports projected to hit \$38\nbillion",
            subtitle: "The Tribune Express",
            time: ". 50m",
            imagepath: "images/export_resize.png",
            onclick: (){
              changeScreen(context, NewsView());
            },
          ),
          SizedBox(
            height: height * 0.02,
          ),
          NewsList(
            title: "Punjab for increasing subsidy on Kissan Cards",
            subtitle: "The Dawn Express",
            time: ". 1h",
            imagepath: "images/punjab.png",
            onclick: (){
              changeScreen(context, NewsView());
            },
          ),
          SizedBox(
            height: height * 0.02,
          ),
          NewsList(
            title: "Punjab for increasing subsidy on Kissan Cards",
            subtitle: "The Geo Express",
            time: ". 5h",
            imagepath: "images/punjab2.png",
            onclick: (){
              changeScreen(context, NewsView());
            },
          ),
          SizedBox(
            height: height * 0.02,
          ),
          NewsList(
            title: "Time to conserve energy on a national level",
            subtitle: "Mettis Global",
            time: ". 1D",
            imagepath: "images/energy.jpg",
            onclick: (){
              changeScreen(context, NewsView());
            },
          ),
          SizedBox(
            height: height * 0.02,
          ),
          NewsList(
            title: "UAE warns of drone threat as it opens",
            subtitle: "Business Recorder",
            time: ". 1D",
            imagepath: "images/uae.jpg",
            onclick: (){
              changeScreen(context, NewsView());
            },
          ),
          SizedBox(
            height: height * 0.02,
          ),
        ],
      ),
    );
  }
}
