import 'package:flutter/material.dart';
import 'package:investors_lounge/static/commons/common.dart';
import 'package:syncfusion_flutter_charts/charts.dart';



class Overview extends StatefulWidget {
  @override
  _OverviewState createState() => _OverviewState();
}

class _OverviewState extends State<Overview> {
  TooltipBehavior _tooltipBehavior;
  double _value1 = 0;
  double _value2 = 0;


  @override
  void initState(){
    _tooltipBehavior = TooltipBehavior(enable: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //HEIGHT-WIDTH
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              child: SfCartesianChart(
                  primaryXAxis: CategoryAxis(),
                  // Chart title
                  //title: ChartTitle(text: 'Half yearly sales analysis'),
                  // Enable legend
                 // legend: Legend(isVisible: true),
                  // Enable tooltip
                  tooltipBehavior: _tooltipBehavior,

                  series: <LineSeries<SalesData, String>>[
                    LineSeries<SalesData, String>(
                        dataSource:  <SalesData>[
                          SalesData('1D', 35),
                          SalesData('1M', 28),
                          SalesData('6M', 34),
                          SalesData('YTD', 32),
                          SalesData('1Y', 40)
                        ],
                        xValueMapper: (SalesData sales, _) => sales.year,
                        yValueMapper: (SalesData sales, _) => sales.sales,
                        // Enable data label
                        dataLabelSettings: DataLabelSettings(isVisible: true)
                    )
                  ]
              ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal:8.0),
            child: Text("Stats", style: TextStyle( fontSize: 15,fontWeight: FontWeight.bold),),
          ),
          Divider(thickness: 0.5, color: Colors.grey,),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("VOLUME", style: TextStyle(color: Colors.grey, fontSize: 12),),
                    SizedBox(height: height*0.01,),
                    Text("33.5M"),
                  ],
                ),
                Spacer(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("VALUE", style: TextStyle(color: Colors.grey, fontSize: 12),),
                    SizedBox(height: height*0.01,),
                    Text("12.5B"),
                  ],
                ),
                Spacer(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text("1 YEAR CHANGE", style: TextStyle(color: Colors.grey, fontSize: 12),),
                    SizedBox(height: height*0.01,),
                    Text("-2.65%", style: TextStyle(color: Colors.red),),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: height*0.04,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal:8.0),
            child: Text("Day's Range", style: TextStyle( fontSize: 15,fontWeight: FontWeight.bold),),
          ),
          Divider(thickness: 0.5, color: Colors.grey,),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Low", style: TextStyle(color: Colors.grey, fontSize: 12),),
                    Text("33,957.2",style: TextStyle(color: primary, fontSize: 12),),
                  ],
                ),
                Spacer(),
                SizedBox(
                  width: width*0.6,
                  child: Slider(
                    min: 0,
                    activeColor: primary,
                    inactiveColor: Colors.greenAccent,
                    divisions: 20,
                    label: _value1.round().toString(),
                    max: 100,
                    value: _value1,
                    onChanged: (value) {
                      setState(() {
                         _value1 = value;
                      });
                    },),
                ),
                Spacer(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("High", style: TextStyle(color: Colors.grey, fontSize: 12),),
                    Text("34,423.52",style: TextStyle(color: primary, fontSize: 12),),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: height*0.04,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal:8.0),
            child: Text("52 Week Range", style: TextStyle( fontSize: 15,fontWeight: FontWeight.bold),),
          ),
          Divider(thickness: 0.5, color: Colors.grey,),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Low", style: TextStyle(color: Colors.grey, fontSize: 12),),
                    Text("33,957.2",style: TextStyle(color: primary, fontSize: 12),),
                  ],
                ),
                Spacer(),
                SizedBox(
                  width: width*0.6,
                  child: Slider(
                    min: 0,
                    activeColor: primary,
                    inactiveColor: Colors.greenAccent,
                    divisions: 20,
                    label: _value1.round().toString(),
                    max: 100,
                    value: _value2,
                    onChanged: (value) {
                      setState(() {
                        _value2 = value;
                      });
                    },),
                ),
                Spacer(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("High", style: TextStyle(color: Colors.grey, fontSize: 12),),
                    Text("34,423.52",style: TextStyle(color: primary, fontSize: 12),),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
class SalesData {
  SalesData(this.year, this.sales);
  final String year;
  final double sales;
}
