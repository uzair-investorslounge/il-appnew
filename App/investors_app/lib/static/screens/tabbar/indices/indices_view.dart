import 'package:flutter/material.dart';
import 'package:investors_lounge/static/commons/common.dart';
import 'package:investors_lounge/static/screens/tabbar/indices/overview.dart';
//import 'package:investors_lounge/static/screens/tabbar/indices/nested_tab.dart';

class IndicesView extends StatefulWidget {
  @override
  _IndicesViewState createState() => _IndicesViewState();
}

class _IndicesViewState extends State<IndicesView> {
  @override
  Widget build(BuildContext context) {
    //HEIGHT-WIDTH
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: primary,
          //change your color here
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.search,
                color: Color(0xff25cd9c),
              ),
              onPressed: () {}),
        ],
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "KSE100",
              style: TextStyle(
                  color: primary, fontSize: 20, fontWeight: FontWeight.bold),
            ),
            Text(
              "KSE100Index",
              style: TextStyle(color: Colors.grey, fontSize: 12),
            ),
          ],
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              SizedBox(
                width: width * 0.05,
              ),
              Icon(
                Icons.arrow_upward,
                size: 22,
                color: primary,
              ),
              SizedBox(
                width: width * 0.01,
              ),
              Text(
                "32,235.65",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                width: width * 0.02,
              ),
              Text(
                "56.02 (+36.2%)",
                style: TextStyle(color: primary, fontSize: 17),
              ),
            ],
          ),
          SizedBox(
            height: height * 0.01,
          ),
          Row(
            children: [
              SizedBox(
                width: width * 0.1,
              ),
              Icon(
                Icons.access_time,
                color: Colors.red,
                size: 17,
              ),
              SizedBox(
                width: width * 0.01,
              ),
              Text(
                "Feb, 21 2022 4:14 pm",
                style: TextStyle(color: Colors.grey, fontSize: 14),
              ),
            ],
          ),
          Divider(
            thickness: 0.5,
            color: Colors.black,
          ),
          DefaultTabController(
              length: 5, // length of tabs
              initialIndex: 0,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Container(
                      child: TabBar(
                        labelColor: primary,
                        labelStyle: TextStyle(fontSize: 16),
                        unselectedLabelColor: Colors.black54,
                        indicatorColor: primary,
                        indicatorPadding:
                            EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                        //labelPadding: EdgeInsets.symmetric(horizontal: 0),
                        isScrollable: true,
                        tabs: [
                          Tab(text: 'Overview'),
                          Tab(text: 'Volume Leaders'),
                          Tab(text: 'Gainers'),
                          Tab(text: 'Losers'),
                          Tab(text: 'Value Leaders'),
                        ],
                      ),
                    ),
                    Container(
                        height: height * 0.6, //height of TabBarView
                        child: TabBarView(children: <Widget>[
                          Overview(),
                          Text("b"),
                          Text("c"),
                          Text("d"),
                          Text("e"),
                        ]))
                  ])),
        ],
      ),
    );
  }
}
