import 'package:flutter/material.dart';
import 'package:investors_lounge/static/commons/common.dart';

class ExclusiveList extends StatelessWidget {
  final String title;
  final String time;
  final String imagePath;
  final VoidCallback onclick;

  const ExclusiveList({
    Key key,
    this.title,
    this.time,
    this.imagePath,
    this.onclick,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Material(
        shadowColor: Colors.white70,
        color: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        elevation: 2.5,
        child: ListTile(
          visualDensity: VisualDensity(horizontal: 0, vertical: 4),
          dense: true,
          hoverColor: primary,
          onTap: onclick,
          title: Padding(
            padding: const EdgeInsets.only(
              top: 8.0,
            ),
            child: Text(
              title,
              style: TextStyle(
                fontSize: 16,
                color: Colors.black87,
              ),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          subtitle: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                time,
                style: TextStyle(color: Colors.grey),
                overflow: TextOverflow.ellipsis,
              ),
              IconButton(
                icon: Icon(Icons.share),
                onPressed: () {},
                iconSize: 18,
                color: Colors.grey,

              ),
            ],
          ),
          trailing: ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: Image.asset(
              imagePath,
            ),
          ),
        ),
      ),
    );
  }
}
