import 'package:flutter/material.dart';
import 'package:investors_lounge/static/commons/common.dart';

class ResearchList extends StatelessWidget {
  final String title;
  final String subtitle;
  final String time;
  final String imagepath;
  final VoidCallback onclick;

  const ResearchList({
    Key key, this.title, this.subtitle, this.time, this.imagepath, this.onclick,

  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Material(

        shadowColor: Colors.white70,
        color: Colors.white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0)),
        elevation: 2.5,
        child: ListTile(

          visualDensity: VisualDensity(horizontal: 0, vertical: 4),
          dense: true,
          hoverColor: primary,
          onTap: onclick,

          title: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Text(
              title,
              style: TextStyle(fontSize: 18), maxLines: 2, overflow: TextOverflow.ellipsis,
            ),
          ),
          subtitle: Row(
            children: <Widget>[
              Text(
                subtitle,
                style: TextStyle(fontSize: 12, color: Colors.grey),
              ),
              Spacer(),
              Text(
                time,
                style: TextStyle(color: Colors.grey),overflow: TextOverflow.ellipsis,
              ),
              IconButton(
                icon: Icon(Icons.share),
                onPressed: () {},
                iconSize: 18,
                color: Colors.grey,
              ),
            ],
          ),
          trailing: ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: Image.asset(
              imagepath,
            ),
          ),
        ),
      ),
    );
  }
}
